import { StatusBar } from 'expo-status-bar';
import Constants from 'expo-constants';
const statusBarHeight = Constants.statusBarHeight
import React from 'react';
import { 
  StyleSheet, 
  Text, 
  View, 
  Image,  
  SafeAreaView, 
  TouchableOpacity, 
  ScrollView, 
  ActivityIndicator
} from 'react-native';
import Axios from 'axios'; 
import {Picker} from '@react-native-community/picker';


export default class App extends React.Component {

  state = {
    products : null,
    total : 0, 
    colors : null,
    selectedColor : 'all',
    loading : false,
  }

  componentDidMount() {
    this.fetchProducts();
  }

  fetchProducts = () => {
    this.setState({
      loading : true
    })
    Axios.get('https://my-json-server.typicode.com/benirvingplt/products/products').then(
      res => {
        let products = res.data;
        this.pickColors(products);
        products = products.map((p: { quantity: number; }) => {
          p.quantity = 0; 
          return p
        });
        
        this.setState({
          products : res.data,
          loading : false
        })
      }
    ).catch(e => {
      console.log('Unable to get data : ', e.message);
    })
  }

  pickColors = (products: any[]) => {
    let colors = ["all"];
    products = products.map((p) => {
      colors.push(p.colour); 
    })

    this.setState({
      colors : colors
    })
  }

  incQuantity = (id : number) => {
    const { products } = this.state; 
    products && products.find((p: { id: number; quantity: number; }) => {
      return p.id === id && (p.quantity = p.quantity + 1);
    });
    this.setState({ products });

    this.calculateTotal();
  }

  decQuantity = (id : number) => {
    const { products } = this.state; 
    products && products.find((p: { id: number; quantity: number; }) => {
      return p.id === id && p.quantity > 0 && (p.quantity = p.quantity - 1);
    });
    this.setState({ products });

    this.calculateTotal();
  } 

  calculateTotal = () => {
    let total = 0; 
    let {products} = this.state;
    products =  products.filter((p: { quantity: number; }) => {
      return p.quantity > 0;
    }); 
  
    total = products.reduce((total,product) => {
      return total + product.quantity * parseFloat(product.price)
    }, total)
    
    this.setState({
      total : total
    })
    //return total; 
  }

  resetQuantity = (products) => {
    products = products.map((p: { quantity: number; }) => {
      p.quantity = 0; 
      return p
    });
    return products; 
  }

  filterByColor = (color) => {
    let products = this.state.products;
    console.log('Item Value ', color);
    if(color === "all") {
      this.setState({selectedColor : color, total : 0 })
      this.fetchProducts()
    } else {
      let filtered_products = products.filter(p =>  p.colour === color)
      filtered_products = this.resetQuantity(filtered_products);
      console.log('Filtered propducts', filtered_products)
      this.setState({
        products : filtered_products, 
        selectedColor : color,
        total : 0
      })
    }
  }

  render () {
    const {products, total, colors,selectedColor, loading} = this.state;
    return (
      <View style={styles.container}>
        <SafeAreaView>
        <ScrollView>
        { colors &&
        <View style = {{marginBottom : 10}}>
          
          <Picker
            selectedValue={selectedColor}
            style={{height: 100, width: 150}}
            onValueChange={(itemValue) => {
                this.filterByColor(itemValue)
              }
            }>
              {colors && colors.map(color => 
              <Picker.Item key={color} label={color} value={color} /> )}
          </Picker>
        </View>}
        
        {loading ? <ActivityIndicator/> :<View>
        {
          products && products.map((product: { id: string | number | undefined; img: any; name: React.ReactNode; price: React.ReactNode; }) => 
            <View
              style={{
                flexDirection: 'row',
                marginTop: 15,
                padding: 15,
                backgroundColor: '#ddd',
              }}
              key={product.id}
            >
              <View style={{ flexDirection: 'row', flex: 3 }}>
                <Image
                  style={{ width: 100, height: 100 }}
                  source={{
                    uri: product.img,
                  }}
                  resizeMode="contain" />
                <View style={{ flexDirection: 'column', width: 100, flex: 2 }}>
                  <Text style={{ padding: 5 }}> {product.name}</Text>
                  <Text style={{ padding: 5 }}>  £ {product.price}</Text>
                </View>
              </View>

              <View style={{
                flexDirection: 'row',
                height: 45,
                alignItems: 'center',
                justifyContent: 'space-evenly',
                flex: 1
              }}>
                <TouchableOpacity onPress = {() => {this.decQuantity(product.id)}} style={styles.buttonStyle}>
                  <Text>-</Text>
                </TouchableOpacity>

                <Text style={{ marginLeft: 5, marginRight: 5 }}>{product.quantity}</Text>
                <TouchableOpacity onPress = {() => {this.incQuantity(product.id)}} style={styles.buttonStyle}>
                  <Text>+</Text>
                </TouchableOpacity>

              </View>
              
              
            </View>
          )
        }
        </View>}

        <View style = {{ padding : 15}}>
          <Text style = {{textAlign  : "right"}}>
            Total : £ {total}
          </Text>
        </View>
        </ScrollView>
        </SafeAreaView>
      </View>
    );
  }
  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingTop : statusBarHeight,
    marginTop : 10
  },

  buttonStyle : {
    backgroundColor : '#fff',
    width : 32, 
    height : 32, 
    borderRadius : 10, 
    alignItems :'center',
    padding : 5,
  }
});
